# baidu订单预测比赛

#### 介绍
MarTech Challenge 用户购买预测

#### 软件架构
基本使用git作为代码交互, 使用基于树模型以及简洁神经网络模型作为核心算法. 
测试和探索代码以notebook形式存在, 具体见notebooks
在执行训练过程,会把一些最佳实践生成为脚本文件, 见scripts文件. 
在探索数据过程中, 会使用和学习不同的可视化以及统计分析技巧. 相关内容会保存在pics文件下.


#### 安装教程

会在开发环境上建立一个通用的conda虚拟环境. 迁移会用conda 相关的机制来做.

#### 使用说明
仅仅是个人学习使用.

#### 参与贡献




#### 比赛地址:
https://aistudio.baidu.com/aistudio/competition/detail/51

